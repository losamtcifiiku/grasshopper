import gunicorn.app.wsgiapp as gunicorn
import flask, os, sys


app = flask.Flask(__name__, static_folder=os.environ["STATIC_DIR"], static_url_path="")


@app.route("/", methods=["GET"])
def index():
    return app.send_static_file("index.html")


@app.route("/api", methods=["GET"])
def api():
    return "Hi, Flask!"


def cli():
    sys.argv = [sys.argv[0], "backend:app", *sys.argv[1:]]
    gunicorn.run()


if __name__ == "__main__":
    app.run(host="::", port=8080)
