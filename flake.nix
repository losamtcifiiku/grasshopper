{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    poetry2nix.url = "github:nix-community/poetry2nix";
    flake-utils.url = "github:numtide/flake-utils";

    poetry2nix.inputs.flake-utils.follows = "flake-utils";
    poetry2nix.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, poetry2nix, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        name = "grasshopper";
        version = "1.0.0";

        pkgs = import nixpkgs {
          inherit system;
          overlays = [ poetry2nix.overlay ];
        };

        inherit (pkgs) stdenv lib;
      in rec {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            git
            git-lfs
            heroku

            (python3.withPackages (p: [
              p.pip
              p.black
              p.flask
              p.gunicorn

            ]))
            python3Packages.poetry

            nodejs-14_x
            nodePackages.node2nix
            nodePackages.pnpm
          ];
        };

        packages = {
          backend =
            pkgs.poetry2nix.mkPoetryApplication { projectDir = ./backend; };

          frontend = let
            nodeModules = (pkgs.callPackage ./frontend {
              nodejs = pkgs.nodejs-14_x;
            }).nodeDependencies;
          in stdenv.mkDerivation {
            name = "frontend";
            src = ./frontend;

            buildPhase = ''
              ln -s ${nodeModules}/lib/node_modules
              export PATH="${nodeModules}/bin:$PATH"
              export NODE_ENV=production
              webpack
            '';

            installPhase = ''
              mkdir $out
              cp -ar dist/. $out
            '';
          };

          combined = pkgs.writeShellScriptBin "combined" ''
            STATIC_DIR=${packages.frontend} ${packages.backend}/bin/backend -w4
          '';

          docker = pkgs.dockerTools.buildLayeredImage {
            inherit name;
            tag = "latest";
            contents = [ pkgs.busybox pkgs.bashInteractive ];
            config = {
              Cmd = [ "${packages.backend}/bin/backend" "-w4" ];
              Env = [ "STATIC_DIR=${packages.frontend}" ];
            };
          };
        };

        defaultPackage = packages.combined;
      });
}
