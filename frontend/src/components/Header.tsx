import {h} from 'preact'
import {Link} from 'preact-router/match'

import '../styles/Header.scss'

export default () => (
  <header>
    <Link className='logo' href='/app/'>
      EpidemInfo
    </Link>
    <nav>
      <Link
        href='/app/'
        className='header-top__navbar-item'
        activeClassName='header-top__navbar-item--active'
      >
        Home
      </Link>
      <Link
        href='/app/article'
        className='header-top__navbar-item'
        activeClassName='header-top__navbar-item--active'
      >
        Idea
      </Link>
      <Link
        href='/app/feed'
        className='header-top__navbar-item'
        activeClassName='header-top__navbar-item--active'
      >
        News
      </Link>
    </nav>
  </header>
)
