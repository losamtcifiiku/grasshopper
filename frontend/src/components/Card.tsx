import {h} from 'preact'
import {Link} from 'preact-router'
import {
  Button,
  ButtonColor,
  ButtonSize,
  ButtonStyle,
} from '../components/Button'
import Icon from 'preact-material-components/Icon'

import '../styles/Card.scss'

export default () => {
  return (
    <div className='card__section'>
      <div className='card__wrapper'>
        <h1 className='card__heading'>Lorem</h1>
        <div className='card__container'>
          <Link href='/app/feed' className='card__container-card'>
            <div className='card__container-cardInfo'>
              <div className='icon'>
                <Icon icon={['code', '2x']} />
              </div>
              <h3>Lorem</h3>
              <h4>ipsum</h4>
              <p>Lorem ipsum</p>
              <ul className='card__container-features'>
                <li>Lorem ipsum</li>
                <li>Lorem ipsum</li>
                <li>Lorem ipsum</li>
              </ul>
              <Button
                buttonStyle={ButtonStyle.Outline}
                buttonSize={ButtonSize.Wide}
                buttonColor={ButtonColor.Primary}
              >
                Lorem
              </Button>
            </div>
          </Link>
          <Link href='/app/feed' className='card__container-card'>
            <div className='card__container-cardInfo'>
              <div className='icon'>
                <Icon>school</Icon>
              </div>
              <h3>Lorem</h3>
              <h4>ipsum</h4>
              <ul className='card__container-features'>
                <li>Lorem ipsum</li>
                <li>Lorem ipsum</li>
                <li>Lorem ipsum</li>
              </ul>
              <Button
                buttonStyle={ButtonStyle.Outline}
                buttonSize={ButtonSize.Wide}
                buttonColor={ButtonColor.Blue}
              >
                Lorem
              </Button>
            </div>
          </Link>
          <Link href='/app/feed' className='card__container-card'>
            <div className='card__container-cardInfo'>
              <div className='icon'>
                <Icon icon={['code', '2x']} />
              </div>
              <h3>Lorem</h3>
              <h4>ipsum</h4>
              <p>Lorem ipsum</p>
              <ul className='card__container-features'>
                <li>Lorem ipsum</li>
                <li>Lorem ipsum</li>
                <li>Lorem ipsum</li>
              </ul>
              <Button
                buttonStyle={ButtonStyle.Outline}
                buttonSize={ButtonSize.Wide}
                buttonColor={ButtonColor.Primary}
              >
                Lorem
              </Button>
            </div>
          </Link>
        </div>
      </div>
    </div>
  )
}
