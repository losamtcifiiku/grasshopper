import {h, FunctionComponent} from 'preact'

import '../styles/Button.scss'

export enum ButtonStyle {
  Primary = 'primary',
  Outline = 'outline',
}

export enum ButtonSize {
  Medium = 'medium',
  Large = 'large',
  Mobile = 'mobile',
  Wide = 'wide',
}

export enum ButtonColor {
  Primary = 'primary',
  Blue = 'blue',
  Red = 'red',
  Green = 'green',
}

interface Props {
  onClick?: (e: Event) => any
  buttonStyle?: ButtonStyle
  buttonSize?: ButtonSize
  buttonColor?: ButtonColor
}

export const Button: FunctionComponent<Props> = ({
  children,
  onClick,
  buttonStyle = ButtonStyle.Primary,
  buttonSize = ButtonSize.Medium,
  buttonColor = ButtonColor.Primary,
}) => {
  return (
    <button
      className={`btn btn--${buttonStyle} btn--${buttonSize} ${buttonColor}`}
      onClick={onClick}
    >
      {children}
    </button>
  )
}
