import {h, Fragment} from 'preact'
import {HeroSection} from '../components/HeroSection'
import Card from '../components/Card'
import {
  homeLayoutObjOne,
  homeLayoutObjTwo,
  homeLayoutObjAfterThree,
} from '../pages/Data/Data'

export default () => (
  <>
    <HeroSection {...homeLayoutObjOne} />
    <HeroSection {...homeLayoutObjTwo} />
    <Card />
    <HeroSection {...homeLayoutObjAfterThree} />
  </>
)
