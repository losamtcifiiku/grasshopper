import {Props} from '../../components/HeroSection'
// @ts-ignore
import statistics2 from 'url:../../images/statistics-2.svg'
// @ts-ignore
import site1 from 'url:../../images/first_site-1.svg'
// @ts-ignore
import top3_last from 'url:../../images/top_document-last-3.svg'

export const homeLayoutObjOne: Props = {
  lightBg: true,
  lightText: false,
  lightTextDesc: false,
  topLine: 'Lorem',
  headLine: 'Lorem ipsum dolor sit',
  description:
    'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet',
  buttonLabel: 'Start now',
  isLeftSided: true,
  src: site1,
  alt: 'Card',
}

export const homeLayoutObjTwo: Props = {
  lightBg: false,
  lightText: true,
  lightTextDesc: true,
  topLine: 'Lorem',
  headLine: 'Lorem ipsum dolor sit',
  description:
    'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet',
  buttonLabel: 'Learn more...',
  src: statistics2,
  alt: 'Stats',
}

export const homeLayoutObjAfterThree: Props = {
  lightBg: false,
  lightText: true,
  lightTextDesc: true,
  topLine: 'Lorem',
  headLine: 'Lorem ipsum dolor sit',
  description:
    'Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet',
  buttonLabel: 'Get started',
  isLeftSided: true,
  src: top3_last,
  alt: 'Top7',
}
