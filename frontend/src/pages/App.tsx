import {h, FunctionComponent} from 'preact'
import Router, {Route} from 'preact-router'
import Header from '../components/Header'
import Footer from '../components/Footer'
import Home from './Home'
import ScrollUp from '../components/ScrollUp'

import '../styles/App.scss'

export const App: FunctionComponent = () => (
  <div id='app'>
    <Header />
    <main>
      <Router>
        <Route path='/app/' component={Home} />
      </Router>
    </main>
    <Footer />
    <ScrollUp />
  </div>
)

export default App
