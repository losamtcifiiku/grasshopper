import HtmlPlugin from 'html-webpack-plugin'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import svelteConfig from './svelte.config.mjs'

const dev = process.env.NODE_ENV !== 'production'

export default {
  mode: dev ? 'development' : 'production',
  entry: './src/app.js',
  devtool: dev && 'source-map',
  output: {
    clean: true,
  },
  module: {
    rules: [
      { test: /\.js$/, exclude: /node_modules/, use: 'babel-loader' },
      { test: /\.ts$/, exclude: /node_modules/, use: 'ts-loader' },
      { test: /\.css$/, use: [MiniCssExtractPlugin.loader, 'css-loader'] },
      {
        test: /\.svelte$/,
        use: { loader: 'svelte-loader', options: svelteConfig },
      },
    ],
  },
  plugins: [new HtmlPlugin({ minify: !dev }), new MiniCssExtractPlugin()],
}
