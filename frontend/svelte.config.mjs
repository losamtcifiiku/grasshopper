import preprocess from 'svelte-preprocess'

const dev = process.env.NODE_ENV !== 'production'

const config = {
  preprocess: preprocess({
    sourceMap: dev,
    defaults: {
      script: 'ts',
    },
  }),
  emitCss: true,
  dev,
}

export default config
